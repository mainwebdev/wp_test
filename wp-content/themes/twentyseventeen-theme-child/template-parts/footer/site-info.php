<div class="site-info">
	<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentyseventeen' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'twentyseventeen' ), 'WordPress' ); ?></a>
</div><!-- .site-info -->
<div class="contacts">
	<a href="tel: +74951234567">Call Us<span>: 0987654321</span></a>
	<a href="mailto: testdomain@mail.to">Email<span>: testdomain@mail.to</span></a>
	<input type="button" value="Contact Us"/>
</div><!-- .contacts-info -->
<div class="container-popup disabled">
	<div class="popup-custom">
		<a href="tel: +74951234567">Call Us<span>: 0987654321</span></a>
		<a href="mailto: testdomain@mail.to">Email<span>: testdomain@mail.to</span></a>
	</div>
</div><!-- .popup -->